//
//  CommentCell.swift
//  ICarousel-effect
//
//  Created by Developer on 8/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var commentLabel: UILabel!
    func bind(comment:Comment){
        
        let attributedUserName = NSMutableAttributedString(string: comment.autorName + " ", attributes:  [NSAttributedStringKey.font :  UIFont(name: "SFProText-Bold", size: 15)!, NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1490196078, green: 0.1490196078, blue: 0.1490196078, alpha: 1)])
        
        let aattributedText = NSAttributedString(string: comment.text + " ", attributes:  [NSAttributedStringKey.font :  UIFont(name: "SFProText-Regular", size: 15)!, NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1490196078, green: 0.1490196078, blue: 0.1490196078, alpha: 1)])
        attributedUserName.append(aattributedText)
        commentLabel.attributedText = attributedUserName
    }
    
}

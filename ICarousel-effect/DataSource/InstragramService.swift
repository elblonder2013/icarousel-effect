//
//  InstragramService.swift
//  ICarousel-effect
//
//  Created by Developer on 8/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//

import Foundation

protocol InstragramService {
    func loadPostsAndHistories( completion:([Post]?,[History]?, _ error:String?)->())
    
}

class InstagramLocalService : InstragramService {
    func loadPostsAndHistories( completion:([Post]?,[History]?, _ error:String?)->()) {
      
        guard let hitoriresUrl = Bundle.main.url(forResource: "Histories", withExtension: "json"),
              let postsUrl = Bundle.main.url(forResource: "Posts", withExtension: "json"),
              let historiesData = try? Data(contentsOf: hitoriresUrl),
              let postsData = try? Data(contentsOf: postsUrl)
        else {
            completion(nil,nil, "Ha courrido un error cargando los datos" )
            return
        }
        
        let decoder = JSONDecoder()
        do{
            let historiesList = try decoder.decode([History].self, from: historiesData)
            let postsList = try decoder.decode([Post].self, from: postsData)
            completion(postsList,historiesList,nil)
        } catch {
            let error = error.localizedDescription
            completion(nil,nil, "Ha courrido un error decodificando los datos" )
        }
    }
}

class InstagramRemoteService : InstragramService {
    func loadPostsAndHistories( completion:([Post]?,[History]?, _ error:String?)->()) {
        
        
    }
}

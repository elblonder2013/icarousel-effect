//
//  PostDetailCell.swift
//  ICarousel-effect
//
//  Created by Developer on 8/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//

import UIKit
import SDWebImage
class PostDetailCell: UITableViewCell {

    
    @IBOutlet weak var usaerNameLabel: UILabel!
    @IBOutlet weak var userLocationLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var postCaptionLabel: UILabel!
    
    func bind(post:Post){
      
        self.usaerNameLabel.text = post.author
        self.userLocationLabel.text = post.autorLocation
        self.postCaptionLabel.text = post.caption
        self.userImageView.sd_setImage(with: URL(string: post.postAuthorImageUrl), completed: nil)
        
    }
    
}

//
//  Comment.swift
//  ICarousel-effect
//
//  Created by Developer on 8/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//

struct Comment : Decodable,Identifiable{
    var id:String
    var text:String //listado de las url de las imagenes a mostrar en las historias
    var autorName:String
}

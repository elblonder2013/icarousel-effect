//
//  Post.swift
//  ICarousel-effect
//
//  Created by Developer on 8/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//

struct Post: Decodable, Identifiable{
    
    var id:String
    var postImageUrl:String
    var postAuthorImageUrl:String
    var caption:String
    var author:String
    var coments:[Comment]
    var autorLocation:String
}

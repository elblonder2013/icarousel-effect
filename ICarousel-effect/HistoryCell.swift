//
//  HistoryCell.swift
//  ICarousel-effect
//
//  Created by Developer on 7/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//

import UIKit
import SDWebImage
class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var circularView:UIView!
    @IBOutlet weak var userImageView:UIImageView!
    @IBOutlet weak var usernameLabel:UILabel!
    var history:History!
    func bind(history:History){
        self.history = history
        usernameLabel.text = history.autorName
        userImageView.sd_setImage(with: URL(string: history.authorPictureUrl), completed: nil)
        
    }
    
}

//
//  WalkThroughCollectionViewCell.swift
//  ICarousel-effect
//
//  Created by Aman Aggarwal on 11/02/18.
//  Copyright © 2018 Aman Aggarwal. All rights reserved.
//

import UIKit
import SDWebImage
class PostCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cntView: UIView!
    @IBOutlet weak var imgvWalkthrough: UIImageView!
    var post:Post!
    func bind(post:Post){
        self.post = post
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        DispatchQueue.main.async {
            
            self.cntView.layer.shadowColor = UIColor.lightGray.cgColor
            self.cntView.layer.shadowOffset = CGSize(width: 0, height: 6)
            self.cntView.layer.shadowOpacity = 0.3
            self.cntView.layer.shadowPath = UIBezierPath(rect: self.cntView.bounds).cgPath
            self.cntView.layer.shouldRasterize = true
            self.imgvWalkthrough.sd_setImage(with: URL(string: self.post.postImageUrl), completed: nil)
        }
    }
    
}

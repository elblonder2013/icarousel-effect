

import UIKit
import SDWebImage
class HomeController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, HomePresenterDelegate {
    func dataLoadedSuccesfull(posts: [Post], histories: [History]) {
        self.postsList = posts
        self.historiesList = histories
        self.historiesTableView.reloadData()
        self.postsCollectionView.reloadData()
        self.currentPage = 0
    }
    
    func errorLoadingData(error: String) {
        
    }
    
    
    
    var presenter = HomePresenter(instagramService: InstagramLocalService())
    @IBOutlet weak var topBarView:UIView!
    @IBOutlet weak var postsCollectionView:UICollectionView!
    @IBOutlet weak var historiesTableView:UITableView!
    @IBOutlet weak var postDetailTableView: UITableView!
    
    var postsList = [Post]()
    var historiesList = [History]()
    var postDetailDatalist = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.delegate = self
        
        postsCollectionView.register(UINib.init(nibName: "PostCell", bundle: nil), forCellWithReuseIdentifier: "PostCellID")
        historiesTableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier:  "HistoryCellID")
        postDetailTableView.register(UINib(nibName: "PostDetailCell", bundle: nil), forCellReuseIdentifier:  "PostDetailCellID")
        postDetailTableView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier:  "CommentCellID")
        postsCollectionView.delegate = self
        postsCollectionView.dataSource = self
        historiesTableView.dataSource = self
        
        postDetailTableView.dataSource = self
        postDetailTableView.tableFooterView = UIView()
        
        let floawLayout = UPCarouselFlowLayout()
        //  floawLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 60.0, height: walkThroughCollectionView.frame.size.height)
        floawLayout.itemSize = CGSize(width:  postsCollectionView.frame.size.width, height: postsCollectionView.frame.size.width)
        floawLayout.scrollDirection = .vertical
        floawLayout.sideItemScale = 0.8
        floawLayout.sideItemAlpha = 1.0
        floawLayout.spacingMode = .fixed(spacing: 5.0)
        postsCollectionView.collectionViewLayout = floawLayout
        presenter.getDataFeed()
        
        
        
        
        
        
    }
    
    //MARK:- UICollectionVIew Delegates and DataSource
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = postsCollectionView.dequeueReusableCell(withReuseIdentifier: "PostCellID", for: indexPath) as! PostCell
        cell.bind(post: postsList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("itm selected == \(indexPath.row)")
        guard let cell = collectionView.cellForItem(at: indexPath)  as? PostCell else { return }
        let realCenter = collectionView.convert(cell.center, to: collectionView.superview)
        let frame = cell.frame
        let imageView  = UIImageView(frame:frame)
        imageView.sd_setImage(with: URL(string: postsList[indexPath.row].postImageUrl), completed: nil)
        imageView.center = realCenter
        imageView.alpha = 0
        self.view.addSubview(imageView)
        
        UIView.animate(withDuration: 0.3) {
            
        } completion: { (succes) in
            
        }
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseIn) {
            imageView.alpha = 1
            imageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.height, height: self.view.frame.size.height)
            self.topBarView.frame.origin.y = -self.topBarView.frame.height
        } completion: { (succes) in
            
        }
        
        
        
        print(frame)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageSide = self.pageSize.height
        let offset = scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
    
    fileprivate var currentPage: Int = 0 {
        didSet {
            print("page at centre = \(currentPage)")
            print(postsList[currentPage])
            //loading the detail data
            self.postDetailDatalist = []
            self.postDetailDatalist.append(postsList[currentPage])
            self.postDetailDatalist.append(contentsOf: self.postsList[currentPage].coments)
            UIView.transition(with: self.postDetailTableView, duration: 0.35, options: .transitionCrossDissolve) {
                self.postDetailTableView.reloadData()
            } completion: { (succes) in
                
            }
        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.postsCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.height += layout.minimumLineSpacing
        return pageSize
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension HomeController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == historiesTableView ? self.historiesList.count : self.postDetailDatalist.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == historiesTableView{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCellID") as? HistoryCell else { return UITableViewCell() }
            cell.bind(history: self.historiesList[indexPath.row])
            return cell
        } else{
            if let post = self.postDetailDatalist[indexPath.row] as? Post, let cell = tableView.dequeueReusableCell(withIdentifier: "PostDetailCellID") as? PostDetailCell{
                cell.bind(post: post)
                return cell
            }
            else if let comment = self.postDetailDatalist[indexPath.row] as? Comment, let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCellID") as? CommentCell{
                cell.bind(comment: comment)
                return cell
            } else{
                return UITableViewCell()
            }
            
        }
        
    }
    
    
}

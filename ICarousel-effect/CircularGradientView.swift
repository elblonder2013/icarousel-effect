//
//  CircularGradientView.swift
//  ICarousel-effect
//
//  Created by Developer on 7/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//
import UIKit

@IBDesignable
class CircularGradientView: UIView {
    
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable var startColor: UIColor? {
        didSet {
            updateGradient()
        }
    }
    
    // the gradient end colour
    @IBInspectable var endColor: UIColor? {
        didSet {
            updateGradient()
        }
    }
    
    // the gradient layer
    private var gradient: CAGradientLayer?
    
    // initializers
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        installGradient()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        installGradient()
    }
    // Create a gradient and install it on the layer
    private func installGradient() {
        // if there's already a gradient installed on the layer, remove it
        if let gradient = self.gradient {
            gradient.removeFromSuperlayer()
        }
        let gradient =  CAGradientLayer()
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0) //addSublayer(gradient)
        self.gradient = gradient
        
        let mask = CAShapeLayer()
      
        mask.path = UIBezierPath(roundedRect: bounds, cornerRadius:  self.frame.height/2).cgPath
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = self.borderWidth
        mask.cornerRadius = self.frame.height/2
        
        gradient.mask = mask
    }
    
    // Update an existing gradient
    private func updateGradient() {
        if let gradient = self.gradient {
            let startColor = self.startColor ?? UIColor.clear
            let endColor = self.endColor ?? UIColor.clear
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            gradient.startPoint =  CGPoint(x: 1, y: 0)
            gradient.endPoint = CGPoint(x: 0, y: 1)
        }
    }
   
}

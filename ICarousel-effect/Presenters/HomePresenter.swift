//
//  HomePresenter.swift
//  ICarousel-effect
//
//  Created by Developer on 8/9/20.
//  Copyright © 2020 Aman Aggarwal. All rights reserved.
//



protocol HomePresenterDelegate {
    func dataLoadedSuccesfull(posts:[Post],histories:[History])
    func errorLoadingData(error:String)
}

class HomePresenter {
    private let instagramService:InstragramService
    var delegate:HomePresenterDelegate?
    
    init(instagramService:InstragramService){
        self.instagramService = instagramService
    }
    
    func getDataFeed(){
        self.instagramService.loadPostsAndHistories { (posts, histories, error) in
            if  let posts = posts ,  let historires = histories  {
                if let delegate = self.delegate{
                    delegate.dataLoadedSuccesfull(posts: posts, histories: historires)
                }
            } else {
                
            }
        }
    }
}
